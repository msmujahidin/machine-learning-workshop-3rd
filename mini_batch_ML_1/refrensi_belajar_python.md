#### Refrensi untuk belajar pemrograman python 

<b>Bahasa Indonesia</b> :
<i>
    <li>
        <ul>https://codesaya.com</ul>
        <ul>http://www.belajarpython.com</ul>
        <ul>http://indorey.blogspot.co.id/2015/12/buku-belajar-python-dalam-bahasa.html</ul>
        <ul>https://drive.google.com/file/d/0B_ZKePry36BiczFBbWVCSGlab2M/view?usp=drive_web </ul>
        <ul>https://klinikpython.wordpress.com/2016/10/04/161001-ini-dia-buku-rujukan-terbaik-untuk-pemula/</ul>
        <ul>http://sakti.github.io/python101/</ul>

        
<b>Bahasa Inggris</b> : <br>
<i>
    <li>
    <ul>http://www.learnpython.org</ul>
    <ul>http://py3readiness.org</ul>
    <ul>https://learnpythonthehardway.org</ul>
    <ul>http://www.tutorialspoint.com/python3/</ul>
    <ul>https://www.sololearn.com/Course/Python/</ul>
    <ul>http://www.oreilly.com/programming/free/ </ul>
    <ul>https://github.com/learnbyexample/scripting_course/blob/master/Python_curated_resources.md </ul>
    <ul>Programming Python : https://goo.gl/a5nMOs</ul>
    <ul>Learning Python : https://goo.gl/BBYlQY</ul>
    </li>
</i>
